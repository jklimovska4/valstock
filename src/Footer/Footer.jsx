import React from "react";
import "./Footer.css"
import footer from "../assets/footer.png"

const Footer = (props) => {
    return (
        <React.Fragment>
            <div className="footerContainer">
                <img className="footerImg" src={footer} />

                <footer className="footer"> </footer>
            </div>
        </React.Fragment>
    )
};

export default Footer;
