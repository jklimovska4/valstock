import React from "react";
// import "./Button.css"

const Button = (props) => {
    return (
        <button onClick={props.onClick} type={props.type} className={props.className}>{props.label.toUpperCase()}</button>
    )
};

export default Button;
