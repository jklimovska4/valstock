

import React from "react";
import "./Header.css"
import logo from "../assets/logo.png"
import Button from "../Button/Button";

const Header = (props) => {
    return (
        <div className="topnav">
            <img className="logo" src={logo} />
            {/* <div className="myAlbumsContainer"> */}

                {localStorage.getItem('password') && localStorage.getItem('username') && <Button label="My albums" className="myAlbums" onClick={props.myAlbumsOpen} />}
            {/* </div> */}
        </div>
    )
};

export default Header;
