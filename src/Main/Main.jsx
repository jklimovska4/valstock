import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate
} from 'react-router-dom';

import React, { useState } from "react";
import Login from "../Login/Login";
import Dashboard from "../Dashboard/Dashboard";
import ImgDetails from "../ImgDetails/ImgDetails";

import { useEffect } from 'react';
import MyAlbums from '../MyAlbums/MyAlbums';
import Header from "../Header/Header";


const Main = (props) => {
    const navigate = useNavigate()

    useEffect(() => {
        if (localStorage.getItem('password') && localStorage.getItem('username')) {
            navigate("/dashboard")
        }
        else {
            navigate("/")
        }
    }, [localStorage.getItem('password')])

    return (
        <React.Fragment>
            <Header myAlbumsOpen={()=>navigate("/myAlbums")}/>

            <Routes>
                <Route exact path='/' element={< Login navigate={navigate} />}></Route>
                <Route exact path='/dashboard' element={<Dashboard navigate={navigate} />}></Route>
                <Route exact path='/dashboard/details' element={<ImgDetails navigate={navigate} />}></Route>
                <Route exact path='/myAlbums' element={<MyAlbums navigate={navigate} />}></Route>
            </Routes>
        </React.Fragment>
    )
};

export default Main;
