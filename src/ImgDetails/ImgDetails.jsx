import React, { useState } from "react";
import { useLocation } from 'react-router-dom';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "./ImgDetails.css"
import { store } from "../store/store";
import { addAlbums } from "../store/actions/dataActions";
import { useEffect } from "react";

const ImgDetails = (props) => {
    const location = useLocation();
    const [showModal, setShowModal] = useState(false)
    const [data, setData] = useState({ name: null, images: [] })
    let arr = []

    const addToAlbum = () => {
        setShowModal(true)
    }

    const modalAction = (albName) => {
        setData((prev) => {
            return ({ name: albName, images: [...prev.images, location.state.url] })
        })
        localStorage.setItem(albName, data);
    }
    useEffect(() => {
        data.name && arr.push(data)
        addAlbums(arr)
    }, [data])

    const download = async () => {
        const originalImage = location.state.url;
        const image = await fetch(originalImage);

        //Split image name
        const nameSplit = originalImage.split("/");
        const duplicateName = nameSplit.pop();

        const imageBlog = await image.blob()
        const imageURL = URL.createObjectURL(imageBlog)
        const link = document.createElement('a')
        link.href = imageURL;
        link.download = "" + duplicateName + "";
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
    };

    const goBack = () => {
        props.navigate("/dashboard")
    }

    return (
        <div className="container">
            <div className="btnContainer">
                <Button className="addToAlbum" label={"Add to album"} onClick={addToAlbum} />
                <Button className="download" label={"Download"} onClick={download} />
            </div>
            <img src={location.state.url} className="img" />

            <div>
                <h4>{"Uploaded by".toUpperCase()}</h4>
                <h1>{location.state.author ? location.state.author : "Unknown"}</h1>
                <h5 className="date">{location.state.date ? location.state.date : "Unknown"}</h5>
            </div>
            <Button className="goBack" label={"Go Back"} onClick={goBack} />

            {showModal && <Modal setOpenModal={setShowModal} image={location.state.url} modalAction={modalAction} />}
        </div>
    )
};

export default ImgDetails;
