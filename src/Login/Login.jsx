import React, { useState } from "react";
import Input from "../Input/Input";
import Button from "../Button/Button";
import Footer from "../Footer/Footer";
import "./Login.css";

const Login = (props) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    // const [isLoggedIn, setIsLoggedIn] = useState(false);

    const onLoginClick = () => {
        localStorage.setItem('username', username);
        localStorage.setItem('password', password);

        props.navigate("/dashboard")
    }

    const onInputChange = (e) => {
        if (e.target.name === "username") {
            setUsername(e.target.value)
        } else if (e.target.name === "password") {
            setPassword(e.target.value)
        }
    }

    return (
        <React.Fragment>
            <div>
                <h1 className="mainLabel">Join our stock community! </h1>
                <p className="paragraph">
                    Download free photos and videos powered by the best photographers.
                </p>
            </div>

            <Input className={"input"} label={"username"} placeholder={"Enter username here..."} type={"text"} onChange={onInputChange} />
            <Input className={"labelPadding"} label={"password"} placeholder={"Enter password here..."} type={"password"} onChange={onInputChange} />
            <Button className={"signIn"} onClick={onLoginClick} label="Log in" />
            <Footer />

        </React.Fragment>
    )
};

export default Login;
