

import React from "react";
import "./Modal.css";
import Button from "../Button/Button";
import { useState, useCallback } from "react";
import { store } from "../store/store";

const Modal = ({ setOpenModal, image, modalAction }) => {

    const [toNewAlbum, setToNewAlbum] = useState(true)
    const [albumName, setAlbumName] = useState("")
    const [dropdownData, setDropDownData] = useState([])

    const inputChange = useCallback((e) => {
        setAlbumName(e.target.value)
    }, [albumName])

    const addToNewAlbum = () => {
        setToNewAlbum(true)
    }

    const addToExistingAlbum = () => {
        setToNewAlbum(false)
        const data = store.getState().imagesReducer.albums.map((item) => {
            return Object.keys(item)
        })
        setDropDownData(data)
    }

    const saveToAlbum = (e) => {
        modalAction(albumName)
        setOpenModal(false)
    }

    return (
        <div className="modalBackground">
            <div className="modalContainer">
                <div className="body">
                    <div className="title">
                        <Button className="newAlbum btn" label={"Create new album"} onClick={addToNewAlbum} />
                        <Button className="newAlbum btn" label={"Add to existing"} onClick={addToExistingAlbum} />
                    </div>
                    {toNewAlbum === true ? <input className="modalInput" type={"text"} placeholder={"Enter title here"} onChange={inputChange} name={"albumName"}></input> :
                        <select name="selectList" className="selectList">
                            {
                                dropdownData.map((key) => <option value={key}>{key}</option>)
                            }
                        </select>
                    }
                    <div className="actionButtons">
                        <Button label={"Cancel"}
                            onClick={() => {
                                setOpenModal(false);
                            }}
                            className="cancelButton"
                        />
                        <Button label={"Save"} className="saveButton" name={"save"} type={"button"} onClick={saveToAlbum} />
                    </div>
                </div>

            </div>
        </div>
    );
}

export default Modal;