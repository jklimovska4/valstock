import rootReducer from "./reducers/rootReducer";
import { createStore } from "redux";
import { Provider } from "react-redux";

// const store = createStore(rootReducer);
export const store = createStore(rootReducer);