import * as actionTypes from "./actionTypes";
import { store } from "../store";

export const setImagesData = (data) => {
    return {
        type: actionTypes.SET_IMAGES_DATA,
        data,
    };
};

export const setAlbums = (data) => {
    return {
        type: actionTypes.SET_ALBUMS,
        data,
    };
};

export const onGetImages = async (page) => {
    try {
        const data = await fetch(`https://picsum.photos/v2/list?page=${page}&limit=100`).then((response) => {
            return response.json();
        }).then((responseJson) => {
            return responseJson
        });

        data.length && store.dispatch(setImagesData({ imagesData: data }));

    } catch (err) {
        alert("Error occured!")
    }
};
let arr = []

export const addAlbums = (data) => {
    arr.push(data)

    data && store.dispatch(setAlbums({ image: arr }));
}