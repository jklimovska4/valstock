
import dataReducer from "./dataReducers";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
    imagesReducer: dataReducer,
});

export default rootReducer;