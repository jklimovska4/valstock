import * as actionTypes from "../actions/actionTypes";
import { combineReducers } from "redux";

const initialState = {
    imagesData: [],
    loading: false,
    albums: [],
};

const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_IMAGES_DATA:
            return {
                ...state,
                imagesData: action.data.imagesData,
            };
        case actionTypes.SET_ALBUMS:            
            return {
                ...state,
                albums: action.data.image,
            };
        default:
            return state;
    }
};

export default dataReducer;

