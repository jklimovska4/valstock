import React, { useCallback, useMemo, useEffect, useRef, useState } from "react";
import "./Dashboard.css"
import PropTypes from 'prop-types';
import { MasonryLayout } from "../MasonryLayout/MasonryLayout";
import { onGetImages } from "../store/actions/dataActions";
import { useSelector, shallowEqual } from 'react-redux';

const Dashboard = (props) => {
    const images = useSelector((state) => state.imagesReducer.imagesData, shallowEqual);
    const listInnerRef = useRef();
    const [page, setPage] = useState(0)

    const infiniteScroll = () => {
        // End of the document reached?
        if (window.innerHeight + document.documentElement.scrollTop
            === document.documentElement.offsetHeight) {

            let newPage = page;
            newPage++;
            setPage(newPage)
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', infiniteScroll);
        onGetImages(page)
    }, [page])

    MasonryLayout.propTypes = {
        columns: PropTypes.number.isRequired,
        gap: PropTypes.number.isRequired,
        children: PropTypes.arrayOf(PropTypes.element),
    };
    MasonryLayout.defaultProps = {
        columns: 2,
        gap: 20,
    };
    const date = new Date()
    return (
        <div className="topContainer">
            <div className="masonryContainer">
                {
                    images.map(key => {
                        const height = Math.ceil((key.height / 100) * 10);
                        return (
                            <div className="masonryItem" key={key.id} >
                                <img src={key.download_url}
                                    alt={key.author}
                                    style={{ height: `${height}px` }}
                                    loading="lazy"
                                    onClick={() => props.navigate("/dashboard/details", {
                                        state:
                                        {
                                            id: 1,
                                            url: key.download_url,
                                            author: key.author,
                                            date: date.toLocaleString('default', {day:'numeric', month: 'short', year: 'numeric' }).toString()
                                        }
                                    })} />
                            </div>

                        )
                    })
                }
            </div>
        </div>
    )
};

export default Dashboard;
