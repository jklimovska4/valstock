import React from "react";
import "./Input.css";

const Input = (props) => {
    return (
        <div className="inputWrapper">
            <label className="label">{props.label}</label>
            <input className="input" type={props.type} placeholder={props.placeholder} onChange={props.onChange} name={props.label}></input>
        </div>
    )
};

export default Input;
