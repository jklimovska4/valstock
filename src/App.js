import './App.css';
import Main from "./Main/Main"
import { BrowserRouter as Router } from 'react-router-dom';
import React from 'react';
import { store } from "./store/store";
import { Provider } from "react-redux";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <Main />
        </Router>
      </div>
    </Provider>
  );
}

export default App;
